#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QLabel>
#include <QProcess>
#include "execu1.h"
#include "execu3.h"
#include "execu5.h"
#include "execu7.h"
#include "execu9.h"
#include "execu11.h"
#include "execu13.h"
#include "execu15.h"
#include "execu17.h"
#include "execu19.h"
#include "execu21.h"
#include "execu23.h"
#include "poids.h"
#include "entete.h"
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstring>
extern "C" {
#include "src/sox.h"
//#include"src/formats.h"
//#include"src/util.h"
#ifdef NDEBUG /* N.B. assert used with active statements so enable always. */
#undef NDEBUG /* Must undef above assert.h or other that might include it. */
#endif

#include "sox.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
}

#include <memory>
#include <string>
#include "app1.h"
#include "app2.h"
#include "app3.h"
#include "app4.h"
#include "app5.h"
#include "app6.h"
#include "app7.h"
#include "app8.h"
#include "app9.h"
#include "app10.h"
#include "app11.h"
#include "app12.h"
#include "app13.h"
#include "app14.h"
#include "app15.h"
#include "app16.h"
#include "app17.h"
#include "app18.h"
#include "app19.h"
#include "app20.h"
#include "app21.h"
#include "app22.h"
#include "app23.h"
#include "app24.h"
#include "app25.h"
#include "app26.h"
#include "app27.h"
#include "app28.h"
#include "app29.h"
#include "app30.h"
#include "app31.h"
#include "app32.h"
#include "app33.h"
#include "app34.h"
#include "app35.h"
#include "app36.h"
#include "app37.h"
#include "app38.h"
#include "app39.h"
#include "app40.h"
#include "app41.h"
#include "app42.h"
#include "app43.h"
#include "app44.h"
#include "app45.h"
#include "app46.h"
#include "app47.h"
#include "app48.h"
#include "app49.h"
#include "app50.h"
#include "app51.h"
#include "app52.h"
#include "app53.h"
#include "app54.h"
#include "app55.h"
#include "app56.h"
#include "app57.h"
#include "app58.h"
#include "app59.h"
#include "app60.h"
#include "app61.h"
#include "app62.h"
#include "app63.h"
#include "app64.h"
#include "app65.h"
#include "app66.h"
#include "app67.h"
#include "app68.h"
#include "app69.h"
#include "app70.h"
#include "app71.h"
#include "app72.h"
#include "app73.h"
#include "app74.h"
#include "app75.h"
#include "app76.h"
#include "app77.h"
#include "app78.h"
#include "app79.h"
#include "app80.h"
#include "app81.h"
#include "app82.h"
#include "app83.h"
#include "app84.h"
#include "app85.h"
#include <bits/stdc++.h>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


int tabb[21],tabb2[3];



void MainWindow::on_spinBox_valueChanged(int arg1)
{
    tabb[1]=arg1;
}


void MainWindow::on_spinBox_2_valueChanged(int arg1)
{
    tabb[2]=arg1;
}


void MainWindow::on_spinBox_3_valueChanged(int arg1)
{
    tabb[3]=arg1;
}


void MainWindow::on_spinBox_4_valueChanged(int arg1)
{
    tabb[4]=arg1;
}


void MainWindow::on_spinBox_5_valueChanged(int arg1)
{
    tabb[5]=arg1;
}


void MainWindow::on_spinBox_6_valueChanged(int arg1)
{
    tabb[6]=arg1;
}


void MainWindow::on_spinBox_7_valueChanged(int arg1)
{
    tabb[7]=arg1;
}


void MainWindow::on_spinBox_8_valueChanged(int arg1)
{
    tabb[8]=arg1;
}


void MainWindow::on_spinBox_9_valueChanged(int arg1)
{
    tabb[9]=arg1;
}


void MainWindow::on_spinBox_10_valueChanged(int arg1)
{
    tabb[10]=arg1;
}


void MainWindow::on_spinBox_11_valueChanged(int arg1)
{
    tabb[11]=arg1;
}


void MainWindow::on_spinBox_12_valueChanged(int arg1)
{
    tabb[12]=arg1;
}


void MainWindow::on_spinBox_13_valueChanged(int arg1)
{
    tabb[13]=arg1;
}


void MainWindow::on_spinBox_14_valueChanged(int arg1)
{
    tabb[14]=arg1;
}


void MainWindow::on_spinBox_15_valueChanged(int arg1)
{
    tabb[15]=arg1;
}


void MainWindow::on_spinBox_16_valueChanged(int arg1)
{
    tabb[16]=arg1;
}


void MainWindow::on_spinBox_17_valueChanged(int arg1)
{
    tabb[17]=arg1;
}


void MainWindow::on_spinBox_18_valueChanged(int arg1)
{
    tabb[18]=arg1;
}


void MainWindow::on_spinBox_19_valueChanged(int arg1)
{
    tabb[19]=arg1;
}


void MainWindow::on_spinBox_20_valueChanged(int arg1)
{
    tabb[20]=arg1;
}


void MainWindow::on_spinBox_21_valueChanged(int arg1)
{
    tabb[21]=arg1;
}


void MainWindow::on_pushButton_clicked()
{
    switch(tabb[1]) {
    case 1:
        app1(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6]);
        break;
    case 2:
        app2(tabb[2],tabb[3],tabb[4]);
        break;
    case 3:
        app3(tabb[2],tabb[3],tabb[4]);
        break;
    case 4:
        app4(tabb[2],tabb[3],tabb[4],tabb[5]);

        break;
    case 5:
        app5(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 6:
        app6(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 7:
        app7(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 8:
        app8(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 9:
        app9(tabb[2],tabb[3],tabb[4],tabb[5]);

        break;
    case 10:
        app10(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 11:
        app11(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 12:
        app12(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6]);
        break;
    case 13:
        app13(tabb[2],tabb[3],tabb[4]);
        break;
    case 14:
        app14(tabb[2],tabb[3],tabb[4],tabb[5]);

        break;
    case 15:
        app15(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 16:
        app16(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 17:
        app17(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6]);
        break;
    case 18:
        app18(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 19:
        app19(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 20:
        app20(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6]);
        break;
    case 21:
        app21(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 22:
        app22(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 23:
        app23(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 24:
        app24(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 25:
        app25(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 26:
        app26(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 27:
        app27(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 28:
        app28(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 29:
        app29(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 30:
        app30(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 31:
        app31(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 32:
        app32(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 33:
        app33(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 34:
        app34(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 35:
        app35(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13],tabb[14]);
        break;
    case 36:
        app36(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13],tabb[14]);
        break;
    case 37:
        app37(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13],tabb[14],tabb[15],tabb[16],tabb[17],tabb[18],tabb[19],tabb[20]);
        break;
    case 38:
        app38(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13],tabb[14],tabb[15],tabb[16],tabb[17],tabb[18],tabb[19],tabb[20]);
        break;
    case 39:
        app39(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13],tabb[14],tabb[15],tabb[16],tabb[17],tabb[18],tabb[19],tabb[20]);
        break;
    case 40:
        app40(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13],tabb[14],tabb[15],tabb[16],tabb[17],tabb[18],tabb[19],tabb[20],tabb[21]);
        break;
    case 41:
        app41(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6]);
        break;
    case 42:
        app42(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 43:
        app43(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 44:
        app44(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 45:
        app45(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 46:
        app46(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 47:
        app47(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 48:
        app48(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 49:
        app49(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 50:
        app50(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8]);
        break;
    case 51:
        app51(tabb[2],tabb[3]);
        break;
    case 52:
        app52(tabb[2],tabb[3]);
        break;
    case 53:
        app53(tabb[2],tabb[3],tabb[4]);
        break;
    case 54:
        app54(tabb[2],tabb[3],tabb[4]);
        break;
    case 55:
        app55(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 56:
        app56(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13],tabb[14],tabb[15],tabb[16],tabb[17],tabb[18]);
        break;
    case 57:
        app57(tabb[2],tabb[3],tabb[4]);
        break;
    case 58:
        app58(tabb[2],tabb[3],tabb[4]);
        break;
    case 59:
        app59(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 60:
        app60(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6]);
        break;
    case 61:
        app61(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6]);
        break;
    case 62:
        app62(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13]);
        break;
    case 63:
        app63(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13],tabb[14],tabb[15]);
        break;
    case 64:
        app64(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 65:
        app65(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6]);
        break;
    case 66:
        app66(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 67:
        app67(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 68:
        app68(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 69:
        app69(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 70:
        app70(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 71:
        app71(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 72:
        app72(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 73:
        app73(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 74:
        app74(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 75:
        app75(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 76:
        app76(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 77:
        app77(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 78:
        app78(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7]);
        break;
    case 79:
        app79(tabb[2],tabb[3],tabb[4],tabb[5],tabb[6],tabb[7],tabb[8],tabb[9],tabb[10],tabb[11],tabb[12],tabb[13]);
        break;
    case 80:
        app80(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 81:
        app81(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 82:
        app82(tabb[2],tabb[3],tabb[4],tabb[5]);
        break;
    case 83:
        app83(tabb[2],tabb[3]);
        break;
    case 84:
        app84(tabb[2],tabb[3]);
        break;
    case 85:
        app85(tabb[2],tabb[3],tabb[4]);
        break;    
    default:
        app85(tabb[2],tabb[3],tabb[4]);
    }
}

void MainWindow::on_spinBox_25_valueChanged(int arg1)
{
    tabb2[2]=arg1 ;
}


void MainWindow::on_spinBox_23_valueChanged(int arg1)
{
    tabb2[0]=arg1;
}

void MainWindow::on_spinBox_24_valueChanged(int arg1)
{
    tabb2[1]=arg1;
}

void MainWindow::on_pushButton_2_clicked()//wav forme 1 : 200 ondes unité de temps entre 20 et 25
{
    execu1(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_13_clicked()//wav forme 2 : 100 ondes unité de temps entre 20 et 25
{
    execu3(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_14_clicked()//wav forme 3
{
    execu5(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_15_clicked()//wav forme 4
{
    execu7(tabb2[0],tabb2[1],tabb2[2]);
}


void MainWindow::on_pushButton_16_clicked()//wav forme 5
{
    execu9(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_17_clicked()//genere wav 6
{
    execu11(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_27_clicked()//genere wav 7
{
    execu13(tabb2[0],tabb2[1],tabb2[2]);
}


void MainWindow::on_pushButton_28_clicked()//genere wav 8
{

    execu15(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_29_clicked()//genere 9 wav
{
    execu17(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_30_clicked()//genere wav 10
{
    execu19(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_33_clicked()//execu avec selection
{
          execu21(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_56_clicked()
{
   execu23(tabb2[0],tabb2[1],tabb2[2]);
}

void MainWindow::on_pushButton_3_clicked()
{
    poids();
}

void MainWindow::on_pushButton_4_released()//ecoute1
{
    //    std::system("sox audiojo.wav -c 2 josox1.ogg && play josox1.ogg chorus 0.5 0.9 50.0 0.4 0.25 2.0 -t 60.0 0.32 0.4 2.3 -t 40.0 0.3 0.3 1.3 -s");
    //std::system("sox audiojo.wav -c 2 josox1.ogg chorus 0.5 0.9 50.0 0.4 0.25 2.0 -t 60.0 0.32 0.4 2.3 -t 40.0 0.3 0.3 1.3 -s");
    // Initialiser libsox
    sox_format_init();


//   const char* sox(const char [105]);
     std::system("sox audiojo.wav -c 2 josox1.ogg lowpass 5000");

    QProcess qprocess1;
  qprocess1.startDetached("play",QStringList()<<"josox1.ogg");
    // qprocess1.startDetached("sox",QStringList()<<"audiojo.wav -c 2 josox1.ogg");
 //   qprocess1.startDetached("play",QStringList()<<"josox1.ogg");
    sox_format_quit();
}


void MainWindow::on_pushButton_5_released()//2
{
    //    std::system("sox audiojo.wav -c 2 josox2.ogg && play josox2.ogg trim 0 30");
    std::system("sox audiojo.wav -c 2 josox2.ogg trim 0 30");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess2;
    qprocess2.startDetached("play",QStringList()<<"josox2.ogg");
    sox_format_quit();
}


void MainWindow::on_pushButton_6_released()
{
//    std::system("sox audiojo.wav -c 2 josox3.ogg && play josox3.ogg gain -3 pad 0 3 reverb");
    std::system("sox audiojo.wav -c 2 josox3.ogg gain -3 pad 0 3 reverb");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess3;
    qprocess3.startDetached("play",QStringList()<<"josox3.ogg");
    sox_format_quit();
}


void MainWindow::on_pushButton_7_released()
{
 //   std::system("sox audiojo.wav -c 2 josox4.ogg && play josox4.ogg tremolo 10");
    std::system("sox audiojo.wav -c 2 josox4.ogg tremolo 10");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess4;
    qprocess4.startDetached("play",QStringList()<<"josox4.ogg");
    sox_format_quit();
}


void MainWindow::on_pushButton_8_released()
{
 //   std::system("sox audiojo.wav -c 2 josox5.ogg && play josox5.ogg stretch 2");
    std::system("sox audiojo.wav -c 2 josox5.ogg stretch 2");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess5;
    qprocess5.startDetached("play",QStringList()<<"josox5.ogg");
    sox_format_quit();
}


void MainWindow::on_pushButton_9_released()
{
 //   std::system("sox audiojo.wav -c 2 josox6.ogg && play josox6.ogg stretch 4");
    std::system("sox audiojo.wav -c 2 josox6.ogg stretch 4");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess6;
    qprocess6.startDetached("play",QStringList()<<"josox6.ogg");
    sox_format_quit();
}


void MainWindow::on_pushButton_10_released()
{
//    std::system("sox audiojo.wav -c 2 josox7.ogg && play josox7.ogg echo 0.8 0.88 60.0 0.4");
    std::system("sox audiojo.wav -c 2 josox7.ogg echo 0.8 0.88 60.0 0.4");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess7;
    qprocess7.startDetached("play",QStringList()<<"josox7.ogg");
    sox_format_quit();
}


void MainWindow::on_pushButton_11_released()
{
//    std::system("sox audiojo.wav -c 2 josox8.ogg && play josox8.ogg  tremolo 20");
    std::system("sox audiojo.wav -c 2 josox8.ogg tremolo 20");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess8;
    qprocess8.startDetached("play",QStringList()<<"josox8.ogg");
    sox_format_quit();

}


void MainWindow::on_pushButton_12_released() //ecoute 9 mix josox9 et josox8
{
 //   std::system("sox -m audiojo.wav josox8.ogg -c 2 josox9.ogg && play josox9.ogg");
    std::system("sox -m audiojo.wav josox8.ogg -c 2 josox9.ogg");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess9;
    qprocess9.startDetached("play",QStringList()<<"josox9.ogg");
    sox_format_quit();

}

void MainWindow::on_pushButton_18_released() //ecoute 10
{
    std::system("sox audiojo.wav -c 2 josox10.ogg tremolo 50");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess10;
    qprocess10.startDetached("play",QStringList()<<"josox10.ogg");
    sox_format_quit();
}
void MainWindow::on_pushButton_19_released()// ecoute 11
{
    std::system("sox audiojo.wav -c 2 josox11.ogg tremolo 5");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess11;
    qprocess11.startDetached("play",QStringList()<<"josox11.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_20_released()// ecoute 12
{
    std::system("sox audiojo.wav -c 2 josox12.ogg speed 1.33");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess12;
    qprocess12.startDetached("play",QStringList()<<"josox12.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_21_released()// ecoute 13
{
    std::system("sox audiojo.wav -c 2 josox13.ogg tempo 0.5 10 20 30");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess13;
    qprocess13.startDetached("play",QStringList()<<"josox13.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_22_released()//ecoute14
{
    std::system("sox audiojo.wav -c 2 josox14.ogg tremolo 15");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess14;
    qprocess14.startDetached("play",QStringList()<<"josox14.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_23_released()//ecoute15
{
    std::system("sox audiojo.wav -c 2 josox15.ogg gain -5 pad 0 5 reverb");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess15;
    qprocess15.startDetached("play",QStringList()<<"josox15.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_24_released()//ecoute16
{
    std::system("sox audiojo.wav -c 2 josox16.ogg speed 0.25");
 //   std::system("sox audiojo.wav -c 2 josox16.ogg speed 2.0");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess16;
    qprocess16.startDetached("play",QStringList()<<"josox16.ogg");
    sox_format_quit();


}

void MainWindow::on_pushButton_25_released()//ecoute17
{
    std::system("sox audiojo.wav -c 2 josox17.ogg speed 0.5");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess17;
    qprocess17.startDetached("play",QStringList()<<"josox17.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_31_clicked() //ecoute 18
{
    std::system("sox audiojo.wav -c 2 josox18.ogg speed 2");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess18;
    qprocess18.startDetached("play",QStringList()<<"josox18.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_32_clicked()//ecoute19
{
    std::system("sox audiojo.wav -c 2 josox19.ogg speed 4");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess19;
    qprocess19.startDetached("play",QStringList()<<"josox19.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_34_clicked() //ecoute 20
{
    std::system("sox audiojo.wav -c 2 josox20.ogg speed 1.5");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess20;
    qprocess20.startDetached("play",QStringList()<<"josox20.ogg");
    sox_format_quit();
}

void MainWindow::on_pushButton_35_clicked() //ecoute 21
{
    std::system("sox audiojo.wav -c 2 josox21.ogg pitch -31.76665363342927165015877324608");
    // Initialiser libsox
    sox_format_init();
    QProcess qprocess21;
    qprocess21.startDetached("play",QStringList()<<"josox21.ogg");
    sox_format_quit();
}

int choix;

void MainWindow::on_spinBox_22_valueChanged(int arg1) //sélectionneur pour la boite à rythmes
{
    switch(arg1) {
    case 1:{
    
    app60(5,120,10,50,50);
    execu5(50,50,120); //execu5(tabb2[0],tabb2[1],tabb2[2]);
    app60(5,120,10,50,50);
    execu5(50,50,120); //execu5(tabb2[0],tabb2[1],tabb2[2]);
    app60(5,120,10,50,50);
    execu5(50,50,120); //execu5(tabb2[0],tabb2[1],tabb2[2]);
    app60(5,120,10,50,50);
    execu13(50,50,120); //execu13(tabb2[0],tabb2[1],tabb2[2]);
    poids();
    choix=1;
        std::system("sox audiojo.wav -c 2 josox26.ogg repeat 5");
    }
    break;
    case 2:{
        app60(5,120,10,50,50);
        execu11(50,50,99); //execu11(tabb2[0],tabb2[1],tabb2[2]);
        app60(5,120,10,50,50);
        execu11(50,50,120); //execu11(tabb2[0],tabb2[1],tabb2[2]);
        app60(5,120,10,50,50);
        execu11(50,50,120); //execu11(tabb2[0],tabb2[1],tabb2[2]);
        app60(5,120,10,50,50);
        execu13(50,50,120); //execu13(tabb2[0],tabb2[1],tabb2[2]);
        poids();
        choix=2;
        std::system("sox audiojo.wav -c 2 josox27.ogg repeat 32");
    }
    break;
    case 3:{
        app63(120,5,115,5,110,5,105,5,100,5,95,5,5,50);
        execu13(50,50,120); //execu13(tabb2[0],tabb2[1],tabb2[2]);
        app63(120,5,115,5,110,5,105,5,100,5,95,5,5,50);
        execu13(50,50,120); //execu13(tabb2[0],tabb2[1],tabb2[2]);
        app63(120,5,115,5,110,5,105,5,100,5,95,5,5,50);
        execu13(50,50,120); //execu13(tabb2[0],tabb2[1],tabb2[2]);
        app63(120,5,115,5,110,5,105,5,100,5,95,5,5,50);
        execu5(50,50,120); //execu5(tabb2[0],tabb2[1],tabb2[2]);
        poids();
        choix=3;
        std::system("sox audiojo.wav -c 2 josox28.ogg repeat 32");
    }
    break;
    case 4:{
        app58(5,120,20);
        execu11(20,20,199); //execu11(tabb2[0],tabb2[1],tabb2[2]);
        app58(5,120,40);
        execu11(40,40,199); //execu11(tabb2[0],tabb2[1],tabb2[2]);
        poids();
        choix=4;
        std::system("sox audiojo.wav -c 2 josox29.ogg repeat 32");
    }
    break;
    case 5:{
        app81(4,4,2,8);
        execu7(8,8,120); //execu7(tabb2[0],tabb2[1],tabb2[2]);
        app81(4,4,2,12);
        execu7(12,12,120); //execu7(tabb2[0],tabb2[1],tabb2[2]);
        app81(4,4,2,16);
        execu7(16,16,120); //execu7(tabb2[0],tabb2[1],tabb2[2]);
        app81(4,4,2,20);
        execu7(20,20,120); //execu7(tabb2[0],tabb2[1],tabb2[2]);
        poids();
        choix=5;
        std::system("sox audiojo.wav -c 2 josox30.ogg repeat 32");
    }
    break;
    case 6:{
        app82(4,4,2,8);
        execu15(8,8,199); //execu15(tabb2[0],tabb2[1],tabb2[2]);
        app82(4,4,2,12);
        execu15(12,12,199); //execu15(tabb2[0],tabb2[1],tabb2[2]);
        app82(4,4,2,16);
        execu15(16,16,199); //execu15(tabb2[0],tabb2[1],tabb2[2]);
        app82(4,4,2,20);
        execu13(20,20,199); //execu15(tabb2[0],tabb2[1],tabb2[2]);
        poids();
        choix=6;
        std::system("sox audiojo.wav -c 2 josox31.ogg repeat 32");
    }
    break;
    case 7:{
        app58(5,120,20);
        execu11(tabb2[0],tabb2[1],tabb2[2]);
        app58(5,120,40);
        execu11(tabb2[0],tabb2[1],tabb2[2]);
        poids();
        choix=7;
        std::system("sox audiojo.wav -c 2 josox32.ogg repeat 32");
    }
    break;


    default:{
        app58(5,120,20);
        execu11(tabb2[0],tabb2[1],tabb2[2]);
        app58(5,120,40);
        execu11(tabb2[0],tabb2[1],tabb2[2]);
        poids();
        std::system("sox audiojo.wav -c 2 josox33.ogg repeat 32");
    }
    }
}


void MainWindow::on_pushButton_26_clicked()
{
// Initialiser libsox
    sox_format_init();
    QProcess qprocess26;

    switch(choix) {
    case 1:{
    qprocess26.startDetached("play",QStringList()<<"josox26.ogg");
    break;
    }
    case 2:{
        qprocess26.startDetached("play",QStringList()<<"josox27.ogg");
        break;
    }
    case 3:{
        qprocess26.startDetached("play",QStringList()<<"josox28.ogg");
        break;
    }
    case 4:{
        qprocess26.startDetached("play",QStringList()<<"josox29.ogg");
        break;
    }
    case 5:{
        qprocess26.startDetached("play",QStringList()<<"josox30.ogg");
        break;
    }
    case 6:{
        qprocess26.startDetached("play",QStringList()<<"josox31.ogg");
        break;
    }
    default:{
       qprocess26.startDetached("play",QStringList()<<"josox33.ogg");
    }
    }

    sox_format_quit();
}


// debut du mixe

void MainWindow::on_pushButton_54_clicked() //mise à zero de l'entete du fichier wav pour commencer nouveau fichier son
{
 entete();
// std::system("sox audiojo.wav -c 2 audiojo.ogg");
}


void MainWindow::on_pushButton_36_clicked() //piste 1 nomme audiojo1.wav
{
std::system("sox audiojo.wav -c 2 audiojo1.wav");
}

void MainWindow::on_pushButton_37_clicked() //piste 2 audiojo2.wav
{
std::system("sox audiojo.wav -c 2 audiojo2.wav");
}

void MainWindow::on_pushButton_38_clicked() //piste3
{
std::system("sox audiojo.wav -c 2 audiojo3.wav");
}


void MainWindow::on_pushButton_39_clicked() //piste 4
{
std::system("sox audiojo.wav -c 2 audiojo4.wav");
}


void MainWindow::on_pushButton_40_clicked() //piste 5
{
std::system("sox audiojo.wav -c 2 audiojo5.wav");
}


void MainWindow::on_pushButton_41_clicked()
{
std::system("sox audiojo.wav -c 2 audiojo6.wav");
}


void MainWindow::on_pushButton_42_clicked()
{
std::system("sox audiojo.wav -c 2 audiojo7.wav");
}


void MainWindow::on_pushButton_43_clicked()
{
std::system("sox audiojo.wav -c 2 audiojo8.wav");
}


void MainWindow::on_pushButton_44_clicked()
{
std::system("sox audiojo.wav -c 2 audiojo9.wav");
}


void MainWindow::on_pushButton_45_clicked()  // mixe piste 1
{
    std::system("sox -m --norm audiojo1.wav audiojo2.wav audiojo.ogg");
}


void MainWindow::on_pushButton_46_clicked() // mixe piste 2
{
    std::system("sox -m --norm audiojo1.wav audiojo2.wav audiojo3.wav audiojo.ogg");
}


void MainWindow::on_pushButton_47_clicked() //mixe piste 3
{
    std::system("sox -m --norm audiojo1.wav audiojo2.wav audiojo3.wav audiojo4.wav audiojo.ogg");
}


void MainWindow::on_pushButton_48_clicked() // mixe piste 4
{
    std::system("sox -m --norm audiojo1.wav audiojo2.wav audiojo3.wav audiojo4.wav audiojo5.wav audiojo.ogg");
}


void MainWindow::on_pushButton_49_clicked() //mixe piste 5
{
    std::system("sox -m --norm audiojo1.wav audiojo2.wav audiojo3.wav audiojo4.wav audiojo5.wav audiojo6.wav audiojo.ogg");
}


void MainWindow::on_pushButton_50_clicked() //mixe piste 6
{
    std::system("sox -m --norm audiojo1.wav audiojo2.wav audiojo3.wav audiojo4.wav audiojo5.wav audiojo6.wav audiojo7.wav audiojo.ogg");
}


void MainWindow::on_pushButton_51_clicked()
{
    std::system("sox -m audiojo7.wav norm audiojo.ogg audiojotmp.ogg && mv audiojotmp.ogg audiojo.ogg");
}


void MainWindow::on_pushButton_52_clicked()
{
    std::system("sox -m audiojo8.wav norm audiojo.ogg audiojotmp.ogg && mv audiojotmp.ogg audiojo.ogg");
}


void MainWindow::on_pushButton_53_clicked()
{
    std::system("sox -m audiojo9.wav compand audiojo.ogg audiojotmp.ogg && mv audiojotmp.ogg audiojo.ogg");
}

void MainWindow::on_pushButton_55_clicked()//jouer le mixe
{
    sox_format_init();
    QProcess qprocess51;
    qprocess51.startDetached("play",QStringList()<<"audiojo.ogg");
    sox_format_quit();
}



