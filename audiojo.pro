QT       += core gui \
            multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++17

LIBS += -L/usr/lib/x86_64-linux-gnu -lsox
LIBS += -L /usr/lib/x86_64-linux-gnu/libsoxr-lsr.so
LIBS += -L /usr/lib/x86_64-linux-gnu/libsox.so.3
LIBS += -L /usr/lib/x86_64-linux-gnu/libsox.so

//LIBS += -L /lib/libsox.3.0.0
//LIBS +=  -L      /lib/libsox1.so
//LIBS +=  -L      /lib/libsoxr-lsr.so.0.1.9 \
//LIBS += -L       /lib/libsoxr.so.0 \
//LIBS += -L        /lib/libsoxr.so.0.1.2

	
//LIBS += /lib/libsox.a

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    app84.cpp \
    app85.cpp \
    entete.cpp \
    execu11.cpp \
    execu13.cpp \
    execu15.cpp \
    execu17.cpp \
    execu19.cpp \
    execu21.cpp \
    execu3.cpp \
    execu5.cpp \
    execu7.cpp \
    execu9.cpp \
    main.cpp \
    mainwindow.cpp \
    app1.cpp \
    app2.cpp \
    app3.cpp \
    app4.cpp \
    app5.cpp \
    app6.cpp \
    app7.cpp \
    app8.cpp \
    app9.cpp \
    app10.cpp \
    app11.cpp \
    app12.cpp \
    app13.cpp \
    app14.cpp \
    app15.cpp \
    app16.cpp \
    app17.cpp \
    app18.cpp \
    app19.cpp \
    app20.cpp \
    app21.cpp \
    app22.cpp \
    app23.cpp \
    app24.cpp \
    app25.cpp \
    app26.cpp \
    app27.cpp \
    app28.cpp \
    app29.cpp \
    app30.cpp \
    app31.cpp \
    app32.cpp \
    app33.cpp \
    app34.cpp \
    app35.cpp \
    app36.cpp \
    app37.cpp \
    app38.cpp \
    app39.cpp \
    app40.cpp \
    app41.cpp \
    app42.cpp \
    app43.cpp \
    app44.cpp \
    app45.cpp \
    app46.cpp \
    app47.cpp \
    app48.cpp \
    app49.cpp \
    app50.cpp \
    app51.cpp \
    app52.cpp \
    app53.cpp \
    app54.cpp \
    app55.cpp \
    app56.cpp \
    app57.cpp \
    app58.cpp \
    app59.cpp \
    app60.cpp \
    app61.cpp \
    app62.cpp \
    app63.cpp \
    app64.cpp \
    app65.cpp \
    app66.cpp \
    app67.cpp \
    app68.cpp \
    app69.cpp \
    app70.cpp \
    app71.cpp \
    app72.cpp \
    app73.cpp \
    app74.cpp \
    app75.cpp \
    app76.cpp \
    app77.cpp \
    app78.cpp \
    app79.cpp \
    app80.cpp \
    app81.cpp \
    app82.cpp \
    app83.cpp \
    execu1.cpp \
    execu23.cpp \
    poids.cpp








HEADERS += \
    app84.h \
    app85.h \
    entete.h \
    execu11.h \
    execu13.h \
    execu15.h \
    execu17.h \
    execu19.h \
    execu21.h \
    execu3.h \
    execu5.h \
    execu7.h \
    execu9.h \
    mainwindow.h \
    app1.h \
    app2.h \
    app3.h \
    app4.h \
    app5.h \
    app6.h \
    app7.h \
    app8.h \
    app9.h \
    app10.h \
    app11.h \
    app12.h \
    app13.h \
    app14.h \
    app15.h \
    app16.h \
    app17.h \
    app18.h \
    app19.h \
    app20.h \
    app21.h \
    app22.h \
    app23.h \
    app24.h \
    app25.h \
    app26.h \
    app27.h \
    app28.h \
    app29.h \
    app30.h \
    app31.h \
    app32.h \
    app33.h \
    app34.h \
    app35.h \
    app36.h \
    app37.h \
    app38.h \
    app39.h \
    app40.h \
    app41.h \
    app42.h \
    app43.h \
    app44.h \
    app45.h \
    app46.h \
    app47.h \
    app48.h \
    app49.h \
    app50.h \
    app51.h \
    app52.h \
    app53.h \
    app54.h \
    app55.h \
    app56.h \
    app57.h \
    app58.h \
    app59.h \
    app60.h \
    app61.h \
    app62.h \
    app63.h \
    app64.h \
    app65.h \
    app66.h \
    app67.h \
    app68.h \
    app69.h \
    app70.h \
    app71.h \
    app72.h \
    app73.h \
    app74.h \
    app75.h \
    app76.h \
    app77.h \
    app78.h \
    app79.h \
    app80.h \
    app81.h \
    app82.h \
    app83.h \
    execu1.h \
    execu23.h \
    poids.h




FORMS += \
    mainwindow.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    lib.qrc

DISTFILES += \
sox.h


