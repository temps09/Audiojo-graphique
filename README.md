# Audiojo Graphique



## Format audio généré

Le but de cette application est d'apporter un outil pour composer de la musique.
Cette application utilise le format audio.jo.
Le format audio.jo est ultra léger car il génère les sons en utilisant les lois physiques des sons.
Il suffit d'indiquer une suite de formes, quelques dizaines d'octets, pour générer des fichiers sons de plusieurs Mo.
Le format audio.jo se présente sous la forme d'un fichier texte, il accorde 4 valeurs entre 5 et 250 par onde.
La première valeur est celle du point bas de l'onde, puis son plateau, puis le point haut de l'onde, puis son plateau.

De nombreux exemples avec capture d'écran sont donnés ici : https://www.letime.net/son/ap/ 

## Lois en science physique

Les représentants du peuple français, constitués en Assemblée nationale, considérant que l'ignorance, l'oubli ou le mépris des droits de l'homme sont les seules causes des malheurs publics. J'énonce ici les principales lois physiques de l'acoustique pour combattre l'ignorance.

1-Le marteau acoustique est une forme qui se dessine avec une brusque et forte variation d'intensité des fronts. Puis qui diminue, une forme de triangle. Nous trouvons en app57 de l'application, tous les sons de guitare, de piano, de drum, de cloche, de tonnerre...

2- La licorne est un lien entre deux formes différente, ce lien de très faible intensité tue les inerties.

3- En 1993 dans mon mémoire de stage j'avais écris que chaque phénomène possède sa propre base de temps. L'acoustique suit les lois physiques, nous pouvons associer plusieurs formes pour cela il faut que chacune des formes ait une base de temps différente des autres formes.

4- Nous pouvons créer la voix humaine de plusieurs manières. Celle que nous observons le plus souvent est celle qui suit deux lois principales. La première est qu'il faut que les ondes varient dans le même sens en intensité. La deuxième est l'accélération qui augmente en centre du front d'onde. Voir la forme 4 ou 5 avec le générateur 8 dans l'application 


## Vidéos qui montrent les formes

https://commons.wikimedia.org/w/index.php?search=audiojo&title=Special:MediaSearch&go=Lire&uselang=fr&type=video


## Construction des sons

1- La structure des lois physiques est de même nature que la langue française. Nous y trouvons plusieurs niveaux d'entendement, ce sont des réactions de réactions, comme des réflexions de réflexions. La brique première est une forme géométrique dessinée dans un fichier texte. Si la brique première est l'équivalent d'un mot de la langue française, celle-ci suit des lois équivalente à l'ortographe. La manière de lier les mots, les formes ce fait à l'aide des générateurs wav, ce sont les générateurs wav qui vont donner un sens à la phrase, donner une sonorité.

2- Les briques premières, sont les formes dessinées dans le fichier texte. Les deux principales sont déjà citées plus haut à savoir la licorne et le marteau acoustique. Pour que le son d'une forme soit cristallin, il faut que la durée des fronts montant des ondes soit constant, et que la durée des fronts descendant soient constant. Ou si variation de durée il faut que la variation soit constante, mais les sons deviennent beaucoup moins interessant. Le même son peut-être écrit d'une multitude de manière en jouant sur les équilibres.

3- Les générateurs wav, relient les points des formes des briques premières, ce qui dessine des ondes. La manière d'aller d'un point à un autre, va créer différentes accélérations. C'est la cinétique de ces accélérations qui va créer l'entendement, qui va créer l'effet, qui va créer le son entendu. Cette cinétique est la résultante de plusieurs lois, comme l'entendement est la résultante de plusieurs réflexions, qui consuisent qu'un mot puisse avoir un sens contraire. Une forme de haute intensité, peut détruire un marteau acoustique, comme couper le son d'une voyelle, peut faire apparaitre une consonne en créant un petit marteau acoustique. Le générateur wav peut ainsi avoir une forme d'accélération particulière dans le front montant et une autre dans le front descendant. En exemple pour une variation en hauteur de 10 unités de pression et en largeur de 20 unité de temps, nous pouvons placer 2 unités de temps pour chaque unité de pression, une vitesse constante, pas d'accélération, ou nous pouvons placer 5 unités de temps dans la première unité de pression, puis 5 unités de temps dans la dernière unité de pression, puis 4 unités de temps dans la deuxième et l'avant dernière unité de pression, 1 seul unité de temps dans les 3 position de pression et rien au millieu, ce qui crée une grande accélération.
 3a- Dans le générateur 1 wav, nous trouvons dans les front montant une écriture qui cherche à avoir une vitesse constante, en s'appuyant sur des plateaux de crète.

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://framagit.org/temps09/Audiojo-graphique.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://framagit.org/temps09/Audiojo-graphique/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
